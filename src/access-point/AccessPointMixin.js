//import ITSBaseMixin from './ITSBaseMixin.js';

function AccessPointMixin(_super_){
  return class AccessPoint extends ITSBaseMixin(_super_ || Object) {
    static get properties(){
      return Object.assign({
        withhow: {
          type: Boolean,
          value: false
        },
        accessPoint: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(){
      super();
    }

    security(){
      if (this.accessPoint) {
        return `${arguments[0]?arguments[0]+"з":"З"}находиться ${(this.accessPoint.secure)?"в межах":"за межами"} КЗ${arguments[1]?arguments[1]:"."}`
      }
    }

    access(){
      if (this.accessPoint && this.accessPoint.accessType) {
        return `${arguments[0]?arguments[0]+"з":"З"}абезпечує ${this.accessPoint.accessType}${arguments[1]?arguments[1]:"."}`
      }
    }

    captionLine(){
      var line = [["accessPoint.through"],["accessPoint.description"]];
      if (this.withhow) line.unshift(this.how());
      return this.describe(line);
    }

    description(){
      return this.describe([
        this.how(),
        this.captionLine(),
        this.security("(",","),
        this.access(" ",").")
      ]);
    }

    how(){
      if (this.accessPoint){
        if (this.accessPoint.source) return "від";
        if (this.accessPoint.sink) return "до";
        return "через"
      }
    }
  }
}
