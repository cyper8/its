function FloorMixin(_super_){
  return class Floor extends ITSBaseMixin(_super_ || Object){

    static get properties(){
      return Object.assign({
        floor: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(args){
      super(args);
    }

    floorFunction(){
      if (this.floor) {
        if (this.floor.function && (this.floor.function !== "")){
          return `${this.floor.function} поверх`
        }
      }
    }

    captionLine(){
      if (this.floor) {
        return `${this.floor.number} поверх`
      }
    }

    description(){
      var f = this.floorFunction();
      return this.describe([
        this.captionLine(),
        f?'('+f+')':"",
        ["floor.description"]
      ])
    }
  }
}
