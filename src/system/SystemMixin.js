function SystemMixin(_super_){
  return class System extends ITSBaseMixin(_super_ || Object){
    static get properties(){
      return Object.assign({
        system: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(){
      super()
    }

    captionLine(){
      if (this.system && this.system.facility){
        return `Система ${this.system.facility}`
      }
    }

    brief(){
      return this.describe([
        this.captionLine(),
        ["system.description"]
      ]);
    }
  }
}
