//import ITSBaseMixin from './ITSBaseMixin.js';

function PartitionMixin(_super_){
  return class Partition extends ITSBaseMixin(_super_ || Object) {

    static get properties(){
      return Object.assign({
        partition: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(args){
      super(args);
    }

    orientation(){
      var o = this.partition;
      return (o.orientation && (o.orientation != ""))?
        `, що ${o.orientation},`:
        "";
    }

    size(){
      if (this.partition){
        var part = this.partition;
        var lines = [];
        [
          ["thickness","товщина"],
          ["width","ширина"],
          ["height","висота"]
        ].forEach(function(d){
          if (part[d[0]]) {
            lines.push(`${d[1]} - ${part[d[0]]}`)
          }
        });
        lines.toList = (function(){
          if (this.length) {
            return this.reduce(function(list,line){
              list+=`<li>${line}</li>`;
              return list;
            },"<ul>")+"</ul>";
          }
        }).bind(lines);
        lines.toInline = (function(){
          if (this.length) {
            return "("+this.reduce(function(str,line){
              return str+`, ${line}`
            })+")";
          }
        }).bind(lines);
        return lines;
      }
    }

    captionLine(){
      return this.describe([
        ["partition.delimiterType","огороджувальна конструкція"],
        this.orientation()
      ])
    }

    description(){
      return this.describe([
        this.captionLine(),
        ["partition.description"]
      ]);
    }

    fullDesc(){
      return this.describe([
        this.description(),
        this.size().toInline()
      ]);
    }
  }
}
