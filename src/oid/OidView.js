class OidView extends ITSBaseMixin(Polymer.Element) {
  static get is() { return 'oid-view'; }
  static get properties(){
    return Object.assign({
      oid:{
        type: Object,
        notify: true
      },
      did: {
        type: String,
        notify: true
      },
      modified: {
        type: Boolean,
        notify: true
      },
      form: {
        type: Boolean,
        observer: "__loadForm"
      },
      route: Object,
      subroute: Object
    },
    super.properties);
  }

  constructor(){
    super()
  }

  __formatDate(datestring){
    if (datestring){
      return datestring;
    }
    else {
      return "____.__.__";
    }
  }

  __getFloor(){
    if (this.oid && (this.oid.floornum != undefined))
    return this.get("oid.bd.floors."+this.oid.floornum+".number");
  }

  __editOid(evt){
    this.__edit("oid");
  }

  __getOidUrl(val){
    if (val && (val != "~")){
      return '/api/v1/oid/get/'+val;
    }
    else if (val && (val == "~")){
      this.set("modified",true);
    }
  }

  __loadForm(f){
    if (f) Polymer.importHref(
      this.resolveUrl("oid-form.html"),null,null,true
    )
  }

  __getOrigin(){
    return window.location.href;
  }
}
