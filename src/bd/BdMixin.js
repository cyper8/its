function BdMixin(_super_){
  return class Bd extends ITSBaseMixin(_super_ || Object) {

    static get properties(){
      return Object.assign({
        bd: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(args){
      super(args);
    }

    captionLine(){
      if (this.bd) {
        return `адмінбудинок по ${this.bd.address}`
      }
      else {
        return "<empty>"
      }
    }

    height(){
      if (this.bd && this.bd.floors && this.bd.floors.length) {
        return `${this.bd.floors.length.toString()}-поверховий`
      }
    }

    description(){
      return this.describe([
        this.height(),
        this.captionLine()
      ]);
    }

    _sortByNumber(a,b){
      var _a = parseInt(a.number)
      , _b = parseInt(b.number);
      if (_a && _b) {
        return _a - _b;
      }
      else {
        return a.number.localeCompare(b.number);
      }
    }

  }
}
