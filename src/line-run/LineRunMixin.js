//import ITSBaseMixin from './ITSBaseMixin.js';

function LineRunMixin(_super_){
  return class LineRun extends ITSBaseMixin(_super_ || Object) {
    static get properties(){
      return Object.assign({
        lineRun: {
          type: Object,
          notify: true
        }
      },
      super.properties);
    }

    constructor(){
      super();
    }

    lineType(){
      if (this.lineRun && this.lineRun.lineType){
        return `прокладені ${this.lineRun.lineType}`
      }
    }

    refCode(){
      if (this.lineRun && this.lineRun.refCode){
        return `(згідно ${this.lineRun.refCode})`
      }
    }

    details(){
      if (this.lineRun && this.lineRun.description){
        return `, ${this.lineRun.description}`
      }
    }

    description(){
      return this.describe(
        [
          this.lineType(),
          this.details(),
          this.refCode()
        ]
      );
    }
  }
}
