//import ITSBaseMixin from './ITSBaseMixin.js';

function RoomMixin(_super_){
  return class Room extends ITSBaseMixin(_super_ || Object) {

    constructor(args){
      super(args);
    }

    static get properties(){
      return Object.assign({
        room: {
          type: Object,
          notify: true
        },
        bd: Object,
        walls: {
          type: Array,
          computed: '__getWalls(bd,room.floornum,room.walls)'
        },
        systems: {
          type: Array,
          computed: '__getSystems(bd,room.floornum,room.systems)'
        }
      },
      super.properties);
    }

    captionLine(){
      return this.describe([
        ["room.function"],
        ["room.belongsTo"],
        this.floornum("(",")")
      ]);
    }

    area(){
      if (this.room && this.room.area) {
        return `${arguments[0]?arguments[0]+"п":"П"}лоща - ${this.room.area}${String.fromCharCode(0x00B2)}${arguments[1]?arguments[1]:"."}`
      }
    }

    height(){
      if (this.room && this.room.height) {
        return `${arguments[0]?arguments[0]+"в":"В"}исота стелі - ${this.room.height}${arguments[1]?arguments[1]:"."}`
      }
    }

    floornum(){
      var f = this.__getFloor();
      if (f) {
        return `${arguments[0]?arguments[0]+"":""}${f.toString()} поверх${arguments[1]?arguments[1]+"":""}`
      }
    }

    description(){
      return this.describe([
        ["room.function"],
        ["room.belongsTo"],
        ["room.description"],
        ".",
        this.area(),
        this.height(),
        this.floornum(null,".")
      ]);
    }

    __getDefault(bd,flr,type){
      if (!type || (type=="")) return [];
      var _bd = bd || this.get("bd");
      var _flr = (flr!=undefined)?flr:this.get("room.floornum");
      var s;
      if (_bd) {
        if (_flr != undefined) {
          if (_bd.floors.length) {
            var f = _bd.floors.filter(function(e){return e.number == _flr.toString()});
            if (f.length) s = f[0][type];
          }
        }
        if (!(s && s.length)) {
          s = _bd[type];
        }
      }
      if (s && s.length) {
        return s
      }
      else return [];
    }

    __defaultWalls(bd,flr){
      return this.__getDefault(bd,flr,"walls");
    }

    __defaultSystems(bd,flr){
      return this.__getDefault(bd,flr,"systems");
    }

    __getWalls(bd,flr,mywalls){
      var w = mywalls || this.get("room.walls");
      if (w && w.length) {
        return w
      }
      else return this.__defaultWalls(bd,flr)
    }

    __getSystems(bd,flr,mysys){
      var s = mysys || this.get("room.systems");
      if (s && s.length) {
        return s
      }
      else return this.__defaultSystems(bd,flr);
    }

    _sortByNumber(a,b){
      var _a = parseInt(a.number)
      , _b = parseInt(b.number);
      if (_a && _b) {
        return _a - _b;
      }
      else {
        return a.number.localeCompare(b.number);
      }
    }

    __getFloor(){
      return this.get("bd.floors."+this.room.floornum+".number");
    }
  }
}
