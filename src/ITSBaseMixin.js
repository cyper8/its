function ITSBaseMixin(_super_){
  var sup = _super_ || Object;
  return class ITSBase extends sup {

    static get properties(){
      return {

      }
    }

    constructor(){
      super();
    }

    DateWrapper(datestr){
      const sep = "/";
      var date = datestr.split(sep).map(function(e){return parseInt(e)});
      date[1]--;
      return {
        changeMonthBy: function changeMonthBy(n){
          var m = ((date[0]*12)+date[1])+n;
          return [
            (Math.floor(m/12)).toString(),
            ((m%12)+1).toString().padStart(2,"0"),
            date[2].toString().padStart(2,"0")
          ].join("/");
        },
        changeYearBy(n){
          return [
            (date[0]+n).toString(),
            (date[1]+1).toString().padStart(2,"0"),
            date[2].toString().padStart(2,"0")
          ].join("/");
        },
        isLaterThan(anotherdate){
          return (new Date(date[0],date[1],date[2]))>anotherdate;
        },
        isEarlierThan(anotherdate){
          return (new Date(date[0],date[1],date[2]))<anotherdate;
        },
        isNoLaterThan(anotherdate){
          return !this.isLaterThan(anotherdate);
        },
        isNoEarlierThan(anotherdate){
          return !this.isEarlierThan(anotherdate);
        }
      }
    }

    __anEmptyBox(set){
      if (set && set.length) return false;
      else return true;
    }

    __todayStr(){
      var now = new Date();
      return `${now.getFullYear()}/${now.getMonth()+1}/${now.getDate()}`
    }

    historyBack(){
      if (this.route) {
        var c = window.location.pathname.split(this.route.prefix)[1].split("/").length-1;
        if (c) {
          window.history.go(-c);
        }
      }
    }

    describe(line){
      var self = this;
      return line.map(function(str){
        if (str instanceof Array){
          var s = Polymer.Path.get(self,str[0]) || "";
          if ((s == "") && str[1]) s = str[1];
          if (s !== "") return s;
        }
        else if (typeof str === "string") return str;
      }).join(" ");
    }

    __add(path,init){
      if (!(this.get(path) instanceof Array)) this.set(path,[]);
      return this.push(path,init)
    }

    __remove(path,index){
      return this.splice(path,index,1);
    }

    static normUrl(path){
      if (path[0] !== "/") {
        path = window.location.pathname+"/"+path;
      }
      while (path.search(/\.\./g) !== -1){
        path = path.replace(/[^\/]+\/\.\.\//g,"")
      }
      path = path.replace(/\.\//g,"/");
      path = path.replace(/\/\//g,"/");
      return (new URL(path,window.location.origin)).href;
    }

    __edit(path){
      window.history.pushState(
        {},
        (arguments[1] || document.title || ""),
        ITSBase.normUrl(path)
      );
      window.dispatchEvent(new CustomEvent("location-changed"));
    }
  }
}
