/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

/* eslint-env node */

module.exports = {
  staticFileGlobs: [
    'index.html',
    'src/*.html',
    'src/*.js',
    'src/**/*',
    'images/*',
    'manifest.json',
    'manifest/*',
    'bower_components/webcomponentsjs/*',
  ],
  navigateFallback: 'index.html',
  runtimeCaching: [{
    urlPattern: /\/api\/v1\/oid/,
    handler: 'networkFirst',
    options: {
      networkTimeoutSeconds: 5,
      cache: {
        maxEntries: 10,
        name: 'oids-cache'
      }
    }
  },{
    urlPattern: /\/api\/v1\/bd/,
    handler: 'networkFirst',
    options: {
      networkTimeoutSeconds: 5,
      cache: {
        maxEntries: 5,
        name: 'bd-cache'
      }
    }
  }]
};
