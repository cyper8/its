var _oids_ = {
  codename: "vp123",
  admbd: {
    street: "Some Str.",
    num: "1a",
    admbd: ""
  },
  officenum: "12",
  floornum: 3,
  belongsTo: {
    type: "сектор",
    description: "мобілізаційної роботи"
  },
  haveAccess: [
    {
      type: "завідувач",
      description: "сектором мобілізаційної роботи"
    },
    {
      type: "головний спеціаліст",
      description: "сектору мобілізаційної роботи Іванов Іван Іванович"
    }
  ],
  rooms: [
    {
      type: "кабінет",
      doors: [
        {
          type: "двері",
          detail: "вхідні",
          description: "дерев'яні подвійні одностулкові, з ущільнювачем по периметру",
          thickness: "250 мм",
          width: "750 мм",
          height: "2100 мм"
        }
      ],
      windows: [
        {
          type: "віконний блок",
          detail: "у двір",
          description: "дерев'яне, подвійне, однокамерний склопакет",
          thickness: "150 мм",
          width: "1420 mm",
          height: "1900 mm",
          parts: [
            {
              type: "вікно",
              detail: "зовнішнє",
              parts: [
                {
                  type: "шибка",
                  detail: "верхня",
                  description: "не відкривається",
                  height: "600 mm"
                },
                {
                  type: "шибка",
                  detail: "нижня ліва",
                  description: "не відкривається",
                  height: "1300 mm",
                  width: "710 mm"
                },
                {
                  type: "шибка",
                  detail: "нижня права",
                  description: "відкривається",
                  height: "1300 mm",
                  width: "710 mm"
                }
              ]
            },
            {
              type: "вікно",
              detail: "внутрішнє",
              parts: [
                {
                  type: "шибка",
                  detail: "верхня",
                  description: "не відкривається",
                  height: "600 mm"
                },
                {
                  type: "шибка",
                  detail: "нижня ліва",
                  description: "не відкривається",
                  height: "1300 mm",
                  width: "710 mm"
                },
                {
                  type: "шибка",
                  detail: "нижня права",
                  description: "відкривається",
                  height: "1300 mm",
                  width: "710 mm"
                }
              ]
            }
          ]
        }
      ],
      walls: [
        {
          type: "стіна",
          detail: "між ОІД та коридором"
        }
      ],
      systems: [

      ]
    }
  ]
}
