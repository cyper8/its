FROM node:boron
ENV POLYMER_CLI_HOME /home/polymer
ARG user=polymer
ARG group=polymer

RUN useradd -d "$POLYMER_CLI_HOME" -U -m -s /bin/bash ${user}

EXPOSE 8080

USER ${user}
RUN mkdir /home/${user}/its
WORKDIR /home/${user}/its
ADD ./build /home/${user}/its/build
ADD ./index.js /home/${user}/its/index.js
ADD ./node_modules /home/${user}/its/node_modules
ENV NODE_ENV=production
ENV PORT=8080
ENV IP=0.0.0.0
ENTRYPOINT NODE_ENV=production node index.js
