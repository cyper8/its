const env = process.env.NODE_ENV || "development";
const hostname = process.env.C9_HOSTNAME || ((env === "development")?"localhost":"localhost");
const IP = process.env.IP || ((env === "development")?"127.0.0.1":"0.0.0.0");
const PORT = process.env.PORT || ((env === "development")?"3000":"8080");
const DB_URL = (env === "development")?process.env.DB_URL||'mongodb://127.0.0.1:27017/local':process.env.DB_URL;
const mongoose = require('mongoose');
const prpl = require("prpl-server");
const prplConf = (env=="production")?require("./build/polymer.json"):{
  entrypoint: "index.html",
  builds: [
    {
      "name":".",
      "basePath":"false"
    }
  ]
};
const express = require('express');
const bodyParser = require('body-parser');
if (!DB_URL) throw new Error('set environment variable DB_URL!');
mongoose.Promise = global.Promise;
var app = express();

var AccessPoint = {
  through: {type: String,default: ""},
  description: {type: String,default: ""},
  accessType: {type: String,default: ""},
  secure: {type: Boolean, default: false},
  source: {type:Boolean,default:false},
  sink: {type: Boolean, default: false}
}

var LineRun = {
  lineType: {type: String,default: ""},
  points: [AccessPoint],
  refCode: {type: String,default: ""},
  description: {type: String,default: ""}
}

var Partition = {
  delimiterType: {type: String,default: ""},
  orientation: {type: String,default: ""},
  description: {type: String,default: ""},
  thickness: {type: String,default: ""},
  width: {type: String,default: ""},
  height: {type: String,default: ""},
  parts: {type:Array,default:[]}// of [Partition]
};

var System = {
  facility: {type: String,default: ""},
  localLines: [LineRun],
  description: {type: String,default: ""},
  responsibilityOf: {type:Array,default:[]}
}

var Room = {
  "function": {type: String,default: ""},
  description: {type: String,default:""},
  belongsTo:{type:String,default:""},
  area: {type: String, default: ""},
  height: {type: String, default: ""},
  floornum: Number,
  walls: [Partition],
  systems: [System]
};

var Floor = {
  number: {type: String, default: "", unique: true, required: true},
  "function": {type: String,default: ""},
  description: {type: String, default:""},
  systems: [System],
  walls: [Partition]
};

var _Admbd = mongoose.Schema({
  address: {type: String,unique:true,required: true},
  systems: {type: Array, default: []},//[System],
  floors: {type: Array, default: []},//[Floor],
  walls: {type: Array, default:[]}//[Partition],
},{collection: "Admbds"});

var _OID = mongoose.Schema({
  codename: {type:String,unique: true,required: true},
  bd: {type:mongoose.Schema.Types.ObjectId,ref:'Admbd'},
  officenum: String,
  floornum: Number,
  suiteName: String,
  belongsTo: String,
  haveAccess: {type:Array,default:[]},
  rooms: {type: Array,default: []},//[Room],
  neighbors:{type: Array,default: []}, //[Room]
  attestation: String,
},{collection:"OIDs"});

mongoose.connect(DB_URL,{useMongoClient:true})
.then(function(db){
  console.log("db ready")
  var Admbd = db.model('Admbd',_Admbd);
  var OID = db.model('OID',_OID);

  app.get('/api/v1/oid/list', function(req,res){
    OID.find({},{codename:1,attestation:1})
      .sort({codename:1})
      .then(function(oids){
        res.json(oids);
        res.end()
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      })
  });

  app.get('/api/v1/bd/list', function(req,res){
    Admbd.find({},{address:1})
      .sort({address:1})
      .then(function(bds){
        res.json(bds);
        res.end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      })
  });

  app.get('/api/v1/oid/get/:id', function(req,res){
    OID.find({_id:req.params.id})
      .populate('bd')
      .then(function(oids){
        res.json(oids[0]);
        res.end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      })
  });

  app.get('/api/v1/bd/get/:id', function(req,res){
    Admbd.find({_id:req.params.id})
      .then(function(bds){
        var bd = bds[0];
        res.json(bd);
        res.end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      })
  });

  app.get('/api/v1/oid/new', function(req,res){
    var newcn = 'ВП';
    OID.find({},{codename:1})
      .then(function(oids){
        newcn += oids.map(function(oid){return parseInt(oid.codename.match(/[0-9]+/)[0])})
        .reduce(function(max,oid){return Math.max(max,oid)},1);
        res.status(200).json(new OID({codename:newcn})).end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      });
  });

  app.post('/api/v1/oid',bodyParser.json(),function(req,res){
    var query={},obj = req.body.oid;
    if (obj._id && (obj._id != "~")) {
      query._id = obj._id;
      delete obj.__v;
    }
    else {
      obj._id = undefined;
      delete obj._id;
      if (obj.codename) {
        query.codename = obj.codename;
      }
      else {
        return res.status(400)
          .json({error:"No query parameter specified."})
          .end();
      }
    }
    OID.findOneAndUpdate(query,obj,{upsert:true,new:true})
      .populate('bd')
      .then(function(doc){
        res.status(200).json(doc).end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      });
  });

  app.post('/api/v1/bd',bodyParser.json(),function(req,res){
    var query={},obj = req.body.bd;
    if (obj._id && (obj._id != "~")) {
      query._id = obj._id;
      delete obj.__v;
    }
    else {
      obj._id = undefined;
      delete obj._id;
      if (obj.address) {
        query.address = obj.address;
      }
      else {
        return res.status(400)
          .json({error:"No query parameter specified."})
          .end();
      }
    }
    Admbd.findOneAndUpdate(query,obj,{upsert:true, new:true})
      .then(function(doc){
        res.status(200).json(doc).end();
      })
      .catch(function(error){
        console.error(error);
        res.status(500).json({error:error}).end();
      });
  });

  app.delete('/api/v1/bd/:id',function(req,res){
    if (req.params.id){
      Admbd.findOneAndRemove({_id:req.params.id})
        .then(function(doc){
          res.status(200).json(doc).end();
        })
        .catch(function(error){
          console.error(error);
          res.status(500).json({error: error});
        });
    }
    else res.status(400).end();
  });

  app.delete('/api/v1/oid/:id',function(req,res){
    if (req.params.id){
      OID.findOneAndRemove({_id:req.params.id})
        .then(function(doc){
          res.status(200).json({deleted:doc}).end();
        })
        .catch(function(error){
          console.error(error);
          res.status(500).json({error: error});
        });
    }
    else res.status(400).end();
  });

  app.get("/*",prpl.makeHandler((env=="production")?'build':'.', prplConf));

  app.listen(PORT,IP,function(){
    console.log("web ready");
  });
})
.catch(function(error){
  console.error(error);
});
